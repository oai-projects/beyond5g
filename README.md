# Helm Charts for Beyond 5G Testing Scenarios

There are two scenarios these charts will demonstrate. The isolation in both scenarios is provided using Kubernetes `networkpolicies` Custom Resource. 

## Testing Environment

Kubernetes cluster is deployed using Minikube. It is a single node cluster. 

CNI used is Calico. 

```console
kubectl version
Client Version: v1.29.1
Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3
Server Version: v1.28.3
kubectl calico version
Client Version:    v3.27.0
Git commit:        711528eeb
Cluster Version:   v3.26.3
Cluster Type:      k8s,bgp,kubeadm,kdd
```

To re-create the whole minikube cluster check the last section. 

Both scenarios use two namespaces. In case they are no present then you need to create

```bash
kubectl create -f namespaces.yaml
```

In the helm-charts isolation is controlled using two global paramters 

- `internalIsolation` (Intra slice isolation): useful for isolating each and every NF from the NFs which are not required to communicate with it. For example: AMF can communicate with NRF, AUSF, CU-CP and SMF. In short AMF should not be able to ping to telnet to UDR SBI port `80`.
- `isolateFromOther` (Inter slice isolation): All intra slice communication but not inter-slice. 

## Scenario I: Use Case I

Two slices completely isolated and can not communicate which each other. 

```
                            Slice 1
                                |
                                |
                                X
                                |
                                |
                            Slice 2
```

Each slice contains all the Core and RAN NFs -->  AMF, SMF, UPF, NRF, UDR, UDM, AUSF, MYSQL, DU, CU-CP, CU-UP, NR-UE


To deploy the scenario with intra-slice isolation 

```bash
helm dependency update scenario1/slice1/
helm dependency update scenario1/slice2/
helm install s1-slice1 scenario1/slice1 -n beyond5g-slice1 --set global.internalIsolation=true --set global.isolateFromOther=false
helm install s1-slice2 scenario1/slice2 -n beyond5g-slice2 --set global.internalIsolation=true --set global.isolateFromOther=false
## check the pods are up and running in both slices (it takes around ~6 mins because the UE waits for the ran setup with some sleep timers)
kubectl get pods -n beyond5g-slice1
kubectl get pods -n beyond5g-slice2
```

<details>
<summary>The output is similar to:</summary>

```console
Saving 12 charts
Deleting outdated charts

Saving 12 charts
Deleting outdated charts

NAME: s1-slice1
LAST DEPLOYED: Tue Feb  6 22:10:42 2024
NAMESPACE: beyond5g-slice1
STATUS: deployed
REVISION: 1
TEST SUITE: None

NAME: s1-slice2
LAST DEPLOYED: Tue Feb  6 22:10:47 2024
NAMESPACE: beyond5g-slice2
STATUS: deployed
REVISION: 1
TEST SUITE: None

NAME                              READY   STATUS    RESTARTS   AGE
oai-amf-7b966dc47c-8pxqq          2/2     Running   0          12m
oai-ausf-5c64977757-swjzh         2/2     Running   0          12m
oai-cu-cp-697d5d546-jbnrk         2/2     Running   0          12m
oai-cu-up-844d59b8d7-5bl6r        2/2     Running   0          12m
oai-du-97f5d6969-82xb5            2/2     Running   0          12m
oai-nr-ue-54bdbdc8d4-4gn6p        1/1     Running   0          12m
oai-nrf-947d9bf86-hq9kv           2/2     Running   0          12m
oai-smf-786c95d497-px9nf          2/2     Running   0          12m
oai-udm-64f46b8888-62bp8          2/2     Running   0          12m
oai-udr-869744545f-84t8q          2/2     Running   0          12m
oai-upf-55754c966b-dbtqg          2/2     Running   0          12m
s1-slice1-mysql-9b4ff7dbf-px8w2   1/1     Running   0          12m

NAME                              READY   STATUS    RESTARTS   AGE
oai-amf-868db689b-62ctg           2/2     Running   0          6m
oai-ausf-5955f97f84-7v2r7         2/2     Running   0          6m
oai-cu-cp-55bd8b8867-27v2x        2/2     Running   0          5m59s
oai-cu-up-5c4cd944db-5skbn        2/2     Running   0          6m
oai-du-5bf4986756-klkqx           2/2     Running   0          6m
oai-nr-ue-55c4cd959c-jb6mz        1/1     Running   0          6m
oai-nrf-7b9c445665-zxgxj          2/2     Running   0          6m
oai-smf-dc6cd687b-hb8fx           2/2     Running   0          6m
oai-udm-56f5dfd59f-nfxd4          2/2     Running   0          6m
oai-udr-6775f59564-jbfkt          2/2     Running   0          5m59s
oai-upf-6698dcd985-9k6q8          2/2     Running   0          6m
s1-slice2-mysql-dfcd64655-j7992   1/1     Running   0          6m
```
</details>

Ping test to see UE from slice1 is connected

```bash
UE_POD=$(kubectl get pods -n beyond5g-slice1 -l app.kubernetes.io/name=oai-nr-ue -o jsonpath='{.items[*].metadata.name}')
kubectl exec -it -n beyond5g-slice1 $UE_POD -- ping 12.1.1.1 -c4
```

Ping test to see UE from slice2 is connected

```bash
UE_POD=$(kubectl get pods -n beyond5g-slice2 -l app.kubernetes.io/name=oai-nr-ue -o jsonpath='{.items[*].metadata.name}')
kubectl exec -it -n beyond5g-slice2 $UE_POD -- ping 12.1.1.1 -c4
```

Connectivity check AMF Slice 1 <<--->> MYSQL Slice 1

```bash
AMF_POD=$(kubectl get pods -n beyond5g-slice1 -l app.kubernetes.io/name=oai-amf -o jsonpath='{.items[*].metadata.name}')
MYSQL_IP=$(kubectl get pods -n beyond5g-slice1 -l app.kubernetes.io/name=s1-slice1-mysql -o jsonpath='{range .items[*]}{@.status.podIP}{end}')
kubectl exec -it -n beyond5g-slice1 $AMF_POD -c tcpdump -- ping $MYSQL_IP -c2
```
<details>
<summary>The output is similar to:</summary>

```console
PING mysql (10.104.113.239): 56 data bytes
^C
--- mysql ping statistics ---
2 packets transmitted, 0 packets received, 100% packet loss
command terminated with exit code 1
```
</details>

AMF should not be able to do telnet on MySQL. 

```bash
kubectl exec -it -n beyond5g-slice1 $AMF_POD -c tcpdump -- telnet mysql 3306
```

Connectivity check AMF Slice 1<<--->> SMF Slice 2


```bash
AMF_POD=$(kubectl get pods -n beyond5g-slice1 -l app.kubernetes.io/name=oai-amf -o jsonpath='{.items[*].metadata.name}')
SMF_IP=$(kubectl get pods -n beyond5g-slice2 -l app.kubernetes.io/name=oai-smf -o jsonpath='{range .items[*]}{@.status.podIP}{end}')
kubectl exec -it -n beyond5g-slice1 $AMF_POD -c tcpdump -- ping $SMF_IP -c2
```

```console
PING oai-smf.beyond5g-slice2 (10.96.156.211): 56 data bytes
^C
--- oai-smf.beyond5g-slice2 ping statistics ---
2 packets transmitted, 0 packets received, 100% packet loss
command terminated with exit code 1
```
</details>


To check the resource consumption for each ns, values will change every 1 min. 

```bash
kubectl top pod -n beyond5g-slice1
kubectl top pod -n beyond5g-slice2
```

To un-deploy the scenario 

```bash
helm uninstall s1-slice1 -n beyond5g-slice1
helm uninstall s1-slice2 -n beyond5g-slice2
```
<details>
<summary>The output is similar to:</summary>

```console
release "s1-slice1" uninstalled
release "s1-slice2" uninstalled
```
</details>

## Scenario I: Use Case II

Two slices non isolated and can communicate with each other. 

```
                            Slice 1
                                |
                                |
                                |
                                |
                                |
                            Slice 2
```

Each slice contains all the Core and RAN NFs -->  AMF, SMF, UPF, NRF, UDR, UDM, AUSF, MYSQL, DU, CU-CP, CU-UP, NR-UE

To deploy the scenario without isolation 

```bash
helm dependency update scenario1/slice1/
helm dependency update scenario1/slice2/
helm install s1-slice1 scenario1/slice1 -n beyond5g-slice1 --set global.internalIsolation=false --set global.isolateFromOther=false
helm install s1-slice2 scenario1/slice2 -n beyond5g-slice2 --set global.internalIsolation=false --set global.isolateFromOther=false
## check the pods are up and running in both slices (it takes around ~5 mins because the UE waits for the ran setup with some sleep timers)
kubectl get pods -n beyond5g-slice1
kubectl get pods -n beyond5g-slice2
```

<details>
<summary>The output is similar to:</summary>

```console
Saving 12 charts
Deleting outdated charts

Saving 12 charts
Deleting outdated charts

NAME: s1-slice1
LAST DEPLOYED: Tue Feb  6 22:10:42 2024
NAMESPACE: beyond5g-slice1
STATUS: deployed
REVISION: 1
TEST SUITE: None

NAME: s1-slice2
LAST DEPLOYED: Tue Feb  6 22:10:47 2024
NAMESPACE: beyond5g-slice2
STATUS: deployed
REVISION: 1
TEST SUITE: None

NAME                              READY   STATUS    RESTARTS   AGE
oai-amf-7b966dc47c-8pxqq          2/2     Running   0          4m45s
oai-ausf-5c64977757-swjzh         2/2     Running   0          4m45s
oai-cu-cp-697d5d546-jbnrk         2/2     Running   0          4m45s
oai-cu-up-844d59b8d7-5bl6r        2/2     Running   0          4m45s
oai-du-97f5d6969-82xb5            2/2     Running   0          4m45s
oai-nr-ue-54bdbdc8d4-4gn6p        1/1     Running   0          4m45s
oai-nrf-947d9bf86-hq9kv           2/2     Running   0          4m45s
oai-smf-786c95d497-px9nf          2/2     Running   0          4m45s
oai-udm-64f46b8888-62bp8          2/2     Running   0          4m45s
oai-udr-869744545f-84t8q          2/2     Running   0          4m45s
oai-upf-55754c966b-dbtqg          2/2     Running   0          4m45s
s1-slice1-mysql-9b4ff7dbf-px8w2   1/1     Running   0          4m45s

NAME                              READY   STATUS    RESTARTS   AGE
oai-amf-868db689b-62ctg           2/2     Running   0          6m
oai-ausf-5955f97f84-7v2r7         2/2     Running   0          6m
oai-cu-cp-55bd8b8867-27v2x        2/2     Running   0          5m59s
oai-cu-up-5c4cd944db-5skbn        2/2     Running   0          6m
oai-du-5bf4986756-klkqx           2/2     Running   0          6m
oai-nr-ue-55c4cd959c-jb6mz        1/1     Running   0          6m
oai-nrf-7b9c445665-zxgxj          2/2     Running   0          6m
oai-smf-dc6cd687b-hb8fx           2/2     Running   0          6m
oai-udm-56f5dfd59f-nfxd4          2/2     Running   0          6m
oai-udr-6775f59564-jbfkt          2/2     Running   0          5m59s
oai-upf-6698dcd985-9k6q8          2/2     Running   0          6m
s1-slice2-mysql-dfcd64655-j7992   1/1     Running   0          6m
```
</details>

Ping test to see UE from slice1 is connected

```bash
UE_POD=$(kubectl get pods -n beyond5g-slice1 -l app.kubernetes.io/name=oai-nr-ue -o jsonpath='{.items[*].metadata.name}')
kubectl exec -it -n beyond5g-slice1 $UE_POD -- ping 12.1.1.1 -c4
```

Ping test to see UE from slice2 is connected

```bash
UE_POD=$(kubectl get pods -n beyond5g-slice2 -l app.kubernetes.io/name=oai-nr-ue -o jsonpath='{.items[*].metadata.name}')
kubectl exec -it -n beyond5g-slice2 $UE_POD -- ping 12.1.1.1 -c4
```


Connectivity check AMF Slice 1 <<--->> MYSQL Slice 1

```bash
AMF_POD=$(kubectl get pods -n beyond5g-slice1 -l app.kubernetes.io/name=oai-amf -o jsonpath='{.items[*].metadata.name}')
kubectl exec -it -n beyond5g-slice1 $AMF_POD -c tcpdump -- telnet mysql 3306
```
<details>
<summary>The output is similar to:</summary>

```console
J
5.7.44/)O:+IW!XkWSvy6-+9-tmysql_native_password

!#08S01Got packets out of orderConnection closed by foreign host
command terminated with exit code 1
```
</details>

Connectivity check AMF Slice 1<<--->> SMF Slice 2


```bash
AMF_POD=$(kubectl get pods -n beyond5g-slice1 -l app.kubernetes.io/name=oai-amf -o jsonpath='{.items[*].metadata.name}')
SMF_IP=$(kubectl get pods -n beyond5g-slice2 -l app.kubernetes.io/name=oai-smf -o jsonpath='{range .items[*]}{@.status.podIP}{end}')
kubectl exec -it -n beyond5g-slice1 $AMF_POD -c tcpdump -- ping $SMF_IP -c2
```

```console
PING 10.244.120.120 (10.244.120.120): 56 data bytes
64 bytes from 10.244.120.120: seq=0 ttl=63 time=0.127 ms
64 bytes from 10.244.120.120: seq=1 ttl=63 time=0.037 ms

--- 10.244.120.120 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max = 0.037/0.082/0.127 ms
```
</details>

To check the resource consumption for each ns, values will change every 1 min. 

```bash
kubectl top pod -n beyond5g-slice1
kubectl top pod -n beyond5g-slice2
```

To un-deploy the scenario 

```bash
helm uninstall s1-slice1 -n beyond5g-slice1
helm uninstall s1-slice2 -n beyond5g-slice2
```
<details>
<summary>The output is similar to:</summary>

```console
release "s1-slice1" uninstalled
release "s1-slice2" uninstalled
```
</details>


## Scenario II: Use Case I

Two slices sharing some common NFs, they are isolated from other slices. AMF from slice1 can communicate with SMF 

```
                            Slice 1 (AMF,SMF, UPF, NRF, UDR, UDM, AUSF, MYSQL, DU, CU-CP, CU-UP, NR-UE)
                                |
                                |
NSSF                            |
                                |
                                |
                            Slice 2 (SMF, UPF, NRF, DU, CU-CP, CU-UP, NR-UE)
```

Each slice contains all the Core and RAN NFs -->  AMF, SMF, UPF, NRF, UDR, UDM, AUSF, MYSQL, DU, CU-CP, CU-UP, NR-UE

To deploy the scenario without isolation 

```bash
helm dependency update scenario2/slice1/
helm dependency update scenario2/slice2/
 #installs in default namespace
helm install nssf oai-nssf
helm install s2-slice1 scenario2/slice1 -n beyond5g-slice1 --set global.internalIsolation=false --set global.isolateFromOther=false
helm install s2-slice2 scenario2/slice2 -n beyond5g-slice2 --set global.internalIsolation=false --set global.isolateFromOther=false
## check the pods are up and running in both slices (it takes around ~5 mins because the UE waits for the ran setup with some sleep timers)
kubectl get pods -n beyond5g-slice1
kubectl get pods -n beyond5g-slice2
```

```bash
kubectl top pod -n beyond5g-slice1
kubectl top pod -n beyond5g-slice2
```

To un-deploy the scenario 

```bash
helm uninstall nssf
helm uninstall s2-slice1 -n beyond5g-slice1
helm uninstall s2-slice2 -n beyond5g-slice2
```
<details>
<summary>The output is similar to:</summary>

```console
release "s2-slice1" uninstalled
release "s2-slice2" uninstalled
```
</details>

## Scenario III: Use Case I

Two slices non isolated and can communicate with each other.

```
                            Slice 1 (AMF, SMF, UPF, NRF, UDR, UDM, AUSF, MYSQL, CU-CP/UP, DU, NR-UE)
                                |
                                |
                                |
                                |
                                |
                            Slice 2 (DU, NR-UE)
```

To deploy the scenario without isolation

```bash
helm dependency update scenario3/slice1/
helm dependency update scenario3/slice2/
helm install s3-slice1 scenario3/slice1 -n beyond5g-slice1 --set global.internalIsolation=false --set global.isolateFromOther=false
helm install s3-slice2 scenario3/slice2 -n beyond5g-slice2 --set global.internalIsolation=false --set global.isolateFromOther=false
## check the pods are up and running in both slices (it takes around ~5 mins because the UE waits for the ran setup with some sleep timers)
kubectl get pods -n beyond5g-slice1
kubectl get pods -n beyond5g-slice2
```



```
helm uninstall s3-slice1 -n beyond5g-slice1
helm uninstall s3-slice2 -n beyond5g-slice2
```







## Create Minikube Cluster

```bash
./create_cluster.sh
```

The script will spin up a minikube kubernetes cluster that can access 8 CPUs and 32G RAM. 
